require 'time'
require 'typhoeus'

module Adjudication
  module Providers
    class Fetcher
      def provider_data
        unless valid_cache?
          response = Typhoeus.get(provider_data_uri)
          File.open(cached_file_location, 'w') do |file|
            file.write(response.body)
          end
        end
        f = File.open(cached_file_location, 'r')
        content = f.read
        f.close
        content
      end

      private

      def valid_cache?
        cache_exists? && up_to_date_cache?
      end

      def cache_exists?
        File.exist?(cached_file_location)
      end

      def up_to_date_cache?
        cached_on > source_data_time
      end

      def cached_on
        File.mtime(cached_file_location)
      rescue Errno::ENOENT
        FileUtils.touch(cached_file_location)
        Time.new 0
      end

      def source_data_time
        response = Typhoeus.head(provider_data_uri)
        Time.parse(response.headers['Last-Modified'])
      end

      # TODO: make these two methods config driven, not hard-coded
      def provider_data_uri
        'http://provider-data.beam.dental/beam-network.csv'
      end

      def cached_file_location
        'tmp/provider_data.csv'
      end
    end
  end
end

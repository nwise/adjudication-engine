require 'csv'

module Adjudication
  module Providers
    class Validator
      attr_accessor(:content, :logger, :providers)

      def self.validate_providers!(csv_provider_data, logger = STDERR)
        new(csv_provider_data, logger).providers
      end

      def initialize(csv_provider_data, logger = STDERR)
        @providers = {}
        @logger = logger
        @content = parse(csv_provider_data)
      end

      def parse(csv_provider_data) # rubocop:disable Metrics/AbcSize
        CSV.parse(csv_provider_data, headers: true) do |row|
          record = { row[2] => {
            first_name:    row[0],
            last_name:     row[1],
            npi:           row[2],
            practice_name: row[3],
            address_1:     row[4],
            address_2:     row[5],
            city:          row[6],
            state:         row[7],
            zip:           row[8],
            specialty:     row[9]
          } }
          add_if_valid(record)
        end
      rescue CSV::MalformedCSVError
        logger.puts("Malformed CSV")
      end

      def add_if_valid(record)
        valid?(record) ? @providers.merge!(record) : logger.puts(record)
      end

      def valid?(record)
        !(/\A\d{10}\Z/ =~ record.keys.first.to_s).nil?
      end
    end
  end
end

require 'adjudication/engine/version'
require 'adjudication/providers'
require 'adjudication/engine/adjudicator'
require 'adjudication/engine/claim'

module Adjudication
  module Engine
    def self.run(claims_data, logger = STDERR)
      # Get Providers
      fetcher = Adjudication::Providers::Fetcher.new
      provider_data = fetcher.provider_data

      # Reject Bad bad providers
      available_providers = Adjudication::Providers::Validator.validate_providers!(provider_data, logger)

      # Iterate through claims adding providers
      claims = claims_data.map { |claim| Adjudication::Engine::Claim.new(claim) }
      # TODO: optimize this
      claims = Adjudication::Engine::Claim.update_providers!(claims, available_providers)

      # Adjudicate --finally :)
      Adjudication::Engine::Adjudicator.adjudicate(claims, logger)
    end
  end
end

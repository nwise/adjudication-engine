module Adjudication
  module Engine
    class Adjudicator
      attr_reader :processed_claims, :logger

      def initialize(logger = STDERR)
        @logger = logger
        @processed_claims = []
      end

      def self.adjudicate(claims, logger = STDERR)
        klass = new(logger)
        klass.adjudicate(claims)
      end

      def adjudicate(claims)
        claims.map do |claim|
          if duplicate?(claim)
            reject!(claim, 'Duplicate', logger)
          elsif claim.provider.nil?
            reject!(claim, 'No Provider', logger)
          else
            pay!(claim, logger)
          end
        end
        @processed_claims
      end

      private

      def pay!(claim, logger)
        claim.pay!(logger)
        @processed_claims << claim
      end

      def reject!(claim, reason, logger = STDERR)
        claim.reject!(reason, logger)
        @processed_claims << claim
      end

      def duplicate?(claim)
        @processed_claims.any? do |c|
          c.start_date == claim.start_date &&
            c.patient['ssn'] == claim.patient['ssn'] &&
            c.procedure_codes == claim.procedure_codes
        end
      end
    end
  end
end

require 'adjudication/engine/claim_line_item'

module Adjudication
  module Engine
    class Claim
      attr_accessor(
        :npi,
        :number,
        :provider,
        :subscriber,
        :patient,
        :start_date,
        :line_items
      )

      def initialize(claim_hash)
        @npi = claim_hash['npi']
        @number = claim_hash['number']
        @provider = claim_hash['provider']
        @subscriber = claim_hash['subscriber']
        @patient = claim_hash['patient']
        @start_date = claim_hash['start_date']
        @line_items = claim_hash['line_items'].map { |x| ClaimLineItem.new(x) }
      end

      def self.update_providers!(claims, providers)
        claims.map do |claim|
          if providers.key?(claim.npi.to_s)
            claim.provider = providers.fetch(claim.npi.to_s)
          end
          claim
        end
      end

      def pay!(logger = STDERR)
        line_items.each do |li|
          if li.ortho?
            li.pay!(li.charged * 0.25)
          elsif li.preventive_and_diagnostic?
            li.pay!(li.charged)
          else
            li.reject!(number, 'Not Covered', logger)
          end
        end
      end

      def procedure_codes
        line_items.map(&:procedure_code).sort
      end

      def reject!(reason = 'Unspecified', logger = STDERR)
        line_items.map { |li| li.reject!(number, reason, logger) }
      end
    end
  end
end

require 'spec_helper'

RSpec.describe Adjudication::Engine do
  subject { Adjudication::Engine }

  let(:claims_data) { JSON.parse(File.open('spec/fixtures/claims.json').read) }
  let(:logger) { double("Logger") }

  describe '.run' do
    it 'processes claims' do
      allow_any_instance_of(Adjudication::Providers::Fetcher).to receive(:valid_cache?).and_return(true)
      allow_any_instance_of(Adjudication::Providers::Fetcher).to receive(:cached_file_location).and_return('spec/fixtures/providers.csv')
      allow(logger).to receive(:puts)
      expect(logger).to receive(:puts).with('Rejected -- Line Item[D0274] -- Claim Number[2017-09-12-553454] -- Reason[No Provider]').once
      expect(logger).to receive(:puts).with('Rejected -- Line Item[D1110] -- Claim Number[2017-09-12-553454] -- Reason[No Provider]').once
      expect(logger).to receive(:puts).with('Rejected -- Line Item[D2140] -- Claim Number[2017-09-10-112494] -- Reason[Not Covered]').once
      expect(logger).to receive(:puts).with('Rejected -- Line Item[D1110] -- Claim Number[2017-09-01-666999] -- Reason[Duplicate]').once
      expect(logger).to receive(:puts).with('Rejected -- Line Item[D0120] -- Claim Number[2017-09-01-666999] -- Reason[Duplicate]').once

      result = subject.run(claims_data, logger)
      expect(result.size).to be(5)
    end
  end

  it 'has a version number' do
    expect(subject::VERSION).not_to be nil
  end
end

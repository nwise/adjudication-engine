require 'pry'
require 'rspec'

RSpec.describe Adjudication::Providers::Validator do
  let(:provider_data) { File.read('spec/fixtures/providers.csv') }
  let(:logger) { double('Logger', puts: nil) }

  describe '.validate_providers!' do
    subject { Adjudication::Providers::Validator }
    it 'returns valid providers' do
      allow(logger).to receive(:puts)
      valid_providers = subject.validate_providers!(provider_data, logger)

      expect(valid_providers.length).to be(5)
    end

    it 'logs invalid providers to STDERR' do
      logger = double('Logger')

      expect(logger).to receive(:puts).exactly(3).times

      subject.validate_providers!(provider_data, logger)
    end
  end

  describe '#valid?' do
    subject { Adjudication::Providers::Validator.new(provider_data, logger) }
    context 'returns true when NPI' do
      it 'is a 10 digit integer' do
        expect(subject.valid?(1_234_567_890 => {})).to be(true)
      end
      it 'is a string with 10 digits' do
        expect(subject.valid?('0123456789' => {})).to be(true)
      end
    end

    context 'returns false when NPI' do
      it 'is an integer with less than 10 digits' do
        expect(subject.valid?(1 => {})).to be(false)
      end
      it 'is a string with less than 10 digits' do
        expect(subject.valid?('1' => {})).to be(false)
      end
      it 'is an integer with more than 10 digits' do
        expect(subject.valid?(12_345_678_900 => {})).to be(false)
      end
      it 'is a string with more than 10 digits' do
        expect(subject.valid?('12345678900' => {})).to be(false)
      end
      it 'is a float' do
        expect(subject.valid?(1.2345678900 => {})).to be(false)
      end
      it 'is a string-like float' do
        expect(subject.valid?('1.2345678900' => {})).to be(false)
      end
    end
  end
end

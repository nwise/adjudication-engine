require 'rspec'

RSpec.describe Adjudication::Providers::Fetcher do
  subject { Adjudication::Providers::Fetcher.new }

  let(:provider_data_uri) { 'http://provider-data.beam.dental/beam-network.csv' }
  let(:local_file) { double('Local File', read: nil, close: nil) }
  let(:cached_file_location) { 'tmp/provider_data.csv' }

  describe '#provider_data' do
    context "when no local provider_data.csv file" do
      it 'it creates a local copy of the file' do
        allow(subject).to receive(:valid_cache?).and_return false

        # Mock external call
        allow(Typhoeus).to receive(:get).with(provider_data_uri)
        expect(File).to receive(:open).with(cached_file_location, 'r').and_return(local_file)
        allow(local_file).to receive(:map)

        expect(File).to receive(:open).with(cached_file_location, 'w')
        puts subject.provider_data
      end
    end

    context "when the local provider_data.csv file is outdated" do
      it 'it creates an updated copy of the file' do
        allow(subject).to receive(:cache_exists?).and_return(true)
        allow(subject).to receive(:up_to_date_cache?).and_return false

        # Mock external call
        allow(Typhoeus).to receive(:get).with(provider_data_uri)
        expect(File).to receive(:open).with(cached_file_location, 'r').and_return(local_file)
        allow(local_file).to receive(:map)

        expect(File).to receive(:open).with(cached_file_location, 'w')
        puts subject.provider_data
      end
    end

    context "when the local provider_data.csv file is NOT outdated" do
      it 'it uses the local copy of the file' do
        allow(subject).to receive(:valid_cache?).and_return(true)

        # Mock external call
        expect(Typhoeus).not_to receive(:get).with(provider_data_uri)
        expect(File).to receive(:open).with(cached_file_location, 'r').and_return(local_file)
        expect(File).not_to receive(:open).with(cached_file_location, 'w')
        allow(local_file).to receive(:map)

        puts subject.provider_data
      end
    end
  end
end

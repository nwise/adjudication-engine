require 'rspec'
require 'json'
require 'adjudication/engine'
require 'pry'

RSpec.describe Adjudication::Engine::Adjudicator do
  subject { Adjudication::Engine::Adjudicator.new(logger) }
  let(:logger) { double('Logger') }
  let(:claim) do
    double('Claim',
           start_date: Time.now,
           patient: { 'ssn' => '123' },
           provider: 'x',
           procedure_codes: %w(code_1 code_2),
           line_items: [line_item])
  end
  let(:line_item) { double('Line Item') }

  describe '#adjudicate' do
    context 'when a claim is a duplicate' do
      let(:duplicate_claims) { [claim, claim] }
      it 'should reject it' do
        allow(claim).to receive(:pay!).once.with(logger).and_return(true)
        expect(claim).to receive(:reject!).once.with('Duplicate', logger)

        subject.adjudicate(duplicate_claims)
      end
    end
    context 'when a claim has no provider' do
      let(:claims) { [claim, providerless_claim] }
      let(:providerless_claim) do
        double('Provoderless Claim',
               start_date: Time.now,
               patient: { 'ssn' => '123' },
               provider: nil,
               procedure_codes: %w(code_1 code_2),
               line_items: [line_item])
      end
      it 'should reject it' do
        allow(claim).to receive(:pay!).once.with(logger).and_return(true)
        expect(providerless_claim).to receive(:reject!).once.with('No Provider', logger)

        subject.adjudicate(claims)
      end
    end
    context 'when a claim should be paid' do
      let(:claims) { [claim, other_claim] }
      let(:other_claim) do
        double('Other Claim',
               start_date: Time.now,
               patient: { 'ssn' => '123' },
               provider: 'x',
               procedure_codes: %w(code_1 code_2),
               line_items: [line_item])
      end
      it 'should pay it' do
        expect(claim).to receive(:pay!).once.with(logger).and_return(true)
        expect(other_claim).to receive(:pay!).once.with(logger).and_return(true)

        subject.adjudicate(claims)
      end
    end
  end
end

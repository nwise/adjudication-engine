require 'rspec'
require 'adjudication/engine'

RSpec.describe Adjudication::Engine::Claim do
  subject { Adjudication::Engine::Claim }

  let(:providers) { { '1' => matched_provider, '2' => other_provider } }
  let(:matched_provider) { { name: 'Matched Provider' } }
  let(:other_provider) { { name: 'Other Provider' } }
  let(:logger) { double("Logger") }

  describe '.update_providers!' do
    it 'adds provider data when it finds a matching provider' do
      claims = [subject.new('npi' => '1', 'line_items' => [])]

      updated_claims = subject.update_providers!(claims, providers)

      expect(updated_claims.first.provider).to eq(matched_provider)
    end

    it 'add nil for "provider" when there is no match' do
      claims = [subject.new('npi' => 'bad', 'line_items' => [])]

      updated_claims = subject.update_providers!(claims, providers)

      expect(updated_claims.first.provider).to be_nil
    end
  end

  describe '#pay!' do
    context 'when a claim should be partially paid' do
      it 'should pay the appropriate line items' do
        paid1 = double('Paid Item 1', ortho?: true, preventive_and_diagnostic?: false, charged: 3)
        paid2 = double('Paid Item 2', ortho?: false, preventive_and_diagnostic?: true, charged: 5)
        rejected = double('Rejected Item', ortho?: false, preventive_and_diagnostic?: false, charged: 7)

        claim = subject.new(
          'line_items' => []
        )
        claim.line_items = [paid1, paid2, rejected]

        expect(paid1).to receive(:pay!)
        expect(paid2).to receive(:pay!)
        expect(rejected).to receive(:reject!)

        claim.pay!(logger)
      end
    end
  end
end
